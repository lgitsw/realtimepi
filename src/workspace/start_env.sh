#!/bin/sh

if [ ! "$1" ]
then
    IMAGE=rpi3-realtime-lite.img
else
    IMAGE=$1
fi

if [ ! "$2" ]
then
    NEWROOT=mount
else
    NEWROOT=$2
fi

#mount proc mount/proc -t proc
[ ! -d "$NEWROOT" ] && echo "$NEWROOT is not a valid directory." && exit 1;
trap "umount \"${NEWROOT}\"/tmp \"${NEWROOT}\"/dev/null \"${NEWROOT}\"/dev/pts \"${NEWROOT}\"/dev/random \"${NEWROOT}\"/dev/shm \"${NEWROOT}\"/dev/urandom \"${NEWROOT}\"/proc" EXIT INT TERM HUP PIPE &&
    mount -o loop,offset=50331648 "${IMAGE}" "${NEWROOT}/" && \
    mount -o loop,offset=4194304 "${IMAGE}" "${NEWROOT}/boot" && \
    mount --bind /tmp "${NEWROOT}/tmp" && \
    mount --bind /dev/null "${NEWROOT}/dev/null" && \
    mount --bind /dev/pts "${NEWROOT}/dev/pts" && \
    mount --bind /dev/random "${NEWROOT}/dev/random" && \
    mount --bind /dev/shm "${NEWROOT}/dev/shm" && \
    mount --bind /dev/urandom "${NEWROOT}/dev/urandom" && \
    mount --bind /proc "${NEWROOT}/proc" && \
    chroot "${NEWROOT}";
